/*
 * PMD/TModels.h
 *
 */

#ifndef GENOTYPELIKELIHOODS_PMD_MODELS_H_
#define GENOTYPELIKELIHOODS_PMD_MODELS_H_

#include <vector>

#include "TReadGroupMap.h"
#include "genometools/Genotypes/Base.h"

#include "genometools/Genotypes/Containers.h"
#include "TSequencedData.h"
#include "TModel.h"

namespace BAM::RGInfo {class TReadGroupInfo;}

namespace GenotypeLikelihoods::PMD {

class TModels {
private:
	std::vector<TWithPMD> _withPMD;
	TNoPMD _noPMD;
	std::vector<TModel*> _pModels;
	void _pool(const BAM::TReadGroupMap& rgMap);

public:
	void initialize(size_t NReadGroups, const BAM::TReadGroupMap& rgMap);
	void initialize(BAM::RGInfo::TReadGroupInfo & RgInfo);

	void reset(size_t rgID) noexcept {
		_pModels[rgID] = &_noPMD;
	}

	// access models
	TModel &model(size_t rgID) noexcept { return *_pModels[rgID]; }
	const TModel &model(size_t rgID) const noexcept { return *_pModels[rgID]; }

	TModel& model(const BAM::TSequencedData &data) noexcept {
		return model(data.readGroupID);
	}
	const TModel& model(const BAM::TSequencedData &data) const noexcept {
		return model(data.readGroupID);
	}

	bool hasPMD() const noexcept { return !_withPMD.empty(); }

	genometools::TBaseLikelihoods P_dij(const BAM::TSequencedData &data, const genometools::TBaseLikelihoods &P_dij_bbar) const noexcept {
		return model(data).P_dij(data, P_dij_bbar);
	}

	genometools::TBaseProbabilities P_bbar(genometools::Base b, const BAM::TSequencedData &data,
							  const genometools::TBaseLikelihoods &P_dij_bbar) const noexcept {
		return model(data).P_bbar(b, data, P_dij_bbar);
	}

	genometools::TBaseProbabilities P_bbar(genometools::Genotype g, const BAM::TSequencedData &data,
							  const genometools::TBaseLikelihoods &P_dij_bbar) const noexcept {
		return model(data).P_bbar(g, data, P_dij_bbar);
	}

	TBaseBaseProbabilities P_b_bbar(genometools::Genotype g, const BAM::TSequencedData &data,
										   const genometools::TBaseLikelihoods &P_dij_bbar) const noexcept {
		return model(data).P_b_bbar(g, data, P_dij_bbar);
	}

	void addToRGInfo(BAM::RGInfo::TReadGroupInfo & RgInfo) const;
	void log(size_t rgID) const;
};
} // namespace GenotypeLikelihoods::PMD

#endif /* GENOTYPELIKELIHOODS_TSEQUENCINGERRORMODELS_H_ */
