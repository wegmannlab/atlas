/*
 * SequencingError/TModels.h
 *
 *  Created on: May 14, 2020
 *      Author: phaentu
 */

#ifndef GENOTYPELIKELIHOODS_TSEQUENCINGERRORMODELS_H_
#define GENOTYPELIKELIHOODS_TSEQUENCINGERRORMODELS_H_

#include <vector>

#include "TReadGroupMap.h"
#include "coretools/Containers/TStrongArray.h"

#include "SequencingError/TModel.h"
#include "TAlignment.h"
#include "TReadGroupInfo.h"

namespace BAM {
class TReadGroups;
struct TSequencedData;
} // namespace BAM

namespace GenotypeLikelihoods::SequencingError  {

//--------------------------------------------------------------------------
// TModels
// Object containing a vector of TReadGroupModels
//--------------------------------------------------------------------------
using RGModels = coretools::TStrongArray<TModel*, BAM::Mate>;
class TModels {
private:
	std::vector<TWithRecal> _withRecal;
	TNoRecal _noRecal;
	std::vector<RGModels> _pModels;
	void _pool(const BAM::TReadGroupMap& rgMap);

public:
	void initialize(size_t NReadGroups, std::string_view RecalString, const BAM::TReadGroupMap& rgMap);
	void initialize(BAM::RGInfo::TReadGroupInfo & RgInfo);

	void reset(size_t rgID, BAM::Mate mate) noexcept { _pModels[rgID][mate] = &_noRecal; }

	// access models
	RGModels &RGModel(size_t rgID) noexcept { return _pModels[rgID]; }
	const RGModels &RGModel(size_t rgID) const noexcept { return _pModels[rgID]; }

	TModel& model(const BAM::TSequencedData &data) noexcept {
		return *RGModel(data.readGroupID)[data.mate()];
	}
	const TModel& model(const BAM::TSequencedData &data) const noexcept {
		return *RGModel(data.readGroupID)[data.mate()];
	}

	bool recalibrates() const noexcept { return !_withRecal.empty(); }

	// calculate error rates
	coretools::PhredInt phredInt(const BAM::TSequencedData &data) const noexcept {
		return model(data).phredInt(data);
	}
	void recalibrate(BAM::TAlignment &aln) const noexcept { RGModel(aln.readGroupId())[aln.mate()]->recalibrate(aln); };
	genometools::TBaseLikelihoods P_dij(const BAM::TSequencedData &data) const noexcept {
		return model(data).P_dij(data);
	}

	void addToRGInfo(BAM::RGInfo::TReadGroupInfo & RgInfo) const;
	void log(size_t rgID) const;
};

} // namespace GenotypeLikelihoods::SequencingError

#endif /* GENOTYPELIKELIHOODS_TSEQUENCINGERRORMODELS_H_ */
