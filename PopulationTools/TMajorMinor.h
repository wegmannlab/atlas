/*
 * TMajorMinor.h
 *
 *  Created on: Nov 5, 2018
 *      Author: phaentu
 */

#ifndef TMAJORMINOR_H_
#define TMAJORMINOR_H_

namespace PopulationTools {

struct TMajorMinor {
	void run();
};

} // namespace PopulationTools

#endif /* TMAJORMINOR_H_ */
