/*
 * TPolymorhhicWindowIdentifier.h
 *
 *  Created on: Feb 14, 2020
 *      Author: wegmannd
 */

#ifndef POPULATIONTOOLS_TPolymorphicWindowIdentifier_H_
#define POPULATIONTOOLS_TPolymorphicWindowIdentifier_H_

namespace PopulationTools{

class TPolymorphicWindowIdentifier{
public:
	void run();
};

} // namespace PopulationTools

#endif /* POPULATIONTOOLS_TPolymorphicWindowIdentifier_H_ */
