/*
 * TAtlasTestRecalibration.cpp
 *
 *  Created on: Jan 22, 2018
 *      Author: linkv
 */

#include "../TestUtilities/TAtlasTestRecalibration.h"


//------------------------------------------
//TAtlasTest_recalSimulation
//------------------------------------------
TAtlasTest_recalSimulation::TAtlasTest_recalSimulation():TAtlasTest(){
	_name = "recalSimulation";
	filenameTag = _testingPrefix + _name;

	maxPhredInt = -1;
	minPhredInt = -1;
	sdphredInt = -1.0;
	meanQual = -1;
};

void TAtlasTest_recalSimulation::setVariables(coretools::TParameters & params, coretools::TLog* Logfile, coretools::TTaskList* TaskList){
	logfile = Logfile;
	taskList = TaskList;

	bamFileName = filenameTag + ".bam";
	meanQual = params.get<int>("recal_meanQual", 25);
	sdphredInt = params.get("recal_sdQual", 10);
	minPhredInt = params.get<int>("recal_minQual", 0);
	maxPhredInt = params.get<int>("recal_maxQual", 42);
	qualityDist = params.get<std::string>("recal_qualityDist", coretools::str::toString("normal(", meanQual, ",", sdphredInt, ")[", minPhredInt, ",", maxPhredInt, "]"));
//	recalParamString = params.get<std::string>("recal_recalParams", "2,0,0.1,0.001,1{20}");
	recalParamString = params.get<std::string>("recal_recalParams","0.908163,0.22877,-0.0160425,0.170256,0.120439,1.50259,1.55807,0.607032,0.775844,1.1983,3.52317,-0.0538213,0.392298,1.07254,1.41819,-0.387901,0.949369,1.17807,1.3996,0.0631075,0.834644,1.08996,2.29066,-0.102391");
	coretools::str::fillContainerFromStringAny(recalParamString, tmpVec, ",", true);
	coretools::str::repeatIndexes(tmpVec, trueParams);
	recalParamsFileName = filenameTag + "_true_recalibrationEM.txt";
	poolRGFileName = filenameTag + "_poolThese.txt";
};

bool TAtlasTest_recalSimulation::run(coretools::TParameters & params, coretools::TLog* Logfile, coretools::TTaskList* TaskList){
	//1) Define variables
	setVariables(params, Logfile, TaskList);

	//2) Write recal params to file
	//-----------------------------
	std::vector<std::string> paramVector;
	coretools::str::fillContainerFromStringAny(recalParamString, paramVector, ",");
	outRecalParams.open(recalParamsFileName.c_str());
	if(!outRecalParams) throw "Failed to open file '" + recalParamsFileName + "'!";

	outRecalParams << "readGroup\tmate\tmodel\tquality\tposition\tcontext\n";
	for(int i=0; i<3; ++i){
		std::string RGName = "RG" + coretools::str::toString(i);
		outRecalParams << RGName << "\tfirst\tqualFuncPosFuncContext\t";
		//quality
		outRecalParams << trueParams[0] << "," << trueParams[1] << "\t";
		//position
		outRecalParams << trueParams[2] << "," << trueParams[3] << "\t";
		//context
		outRecalParams << trueParams[4];
		for(unsigned int p=5; p<paramVector.size(); ++p)
			outRecalParams << "," << paramVector[p] ;
		//add likelihood
		outRecalParams << "\n";
	}
	outRecalParams.close();

	//2) Run ATLAS to simulate BAM file
	//-----------------------------
	//TODO: find minimal data necessary to run test in order to speed up

	_testParams.addParameter("out", filenameTag);
	_testParams.addParameter("chrLength", "2000000");
	_testParams.addParameter("ploidy", "1");
	_testParams.addParameter("depth", "4");
	_testParams.addParameter("qualityDist", qualityDist);
	_testParams.addParameter("recal", recalParamsFileName);
//	_testParams.addParameter("recal", "recal[" + recalParamString + "]");
//	_testParams.addParameter("readLength", "gamma(" + toString(alpha) + "," + toString(beta)+ ")[" + toString(minReadLen) + "," + toString(maxReadLen));
	_testParams.addParameter("readLength", "single:fixed(70)");

	if(!runMain("simulate"))
		return false;

	logfile->newLine();

	//3) Run recal
	//-----------------------------
	//open pool read group file
	outRecalPool.open(poolRGFileName.c_str());
	if(!outRecalPool) throw "Failed to open file '" + poolRGFileName + "'!";
	outRecalPool << "RG0 RG1\n";
	outRecalPool.close();

	_testParams.clear();
	_testParams.addParameter("bam", bamFileName);
	_testParams.addParameter("poolReadGroups", poolRGFileName);


	if(!runMain("recal"))
		return false;

	//4) check if results are OK
	//--------------------------
	if(checkRecalFile() == true)
		return true;
	else return false;
};

bool TAtlasTest_recalSimulation::checkRecalFile(){
	logfile->startIndent("Checking recal file:");

	//open quality file
	std::string filename = filenameTag + "_recalibrationEM.txt";
	logfile->listFlush("Opening file '" + filename + "' for reading ...");
	std::ifstream in(filename.c_str());
	if(!in)
		throw "Failed to open file '" + filename + "'!";
	logfile->done();

	//some variables
	std::string tmp, tmp2;
	std::vector<double> estimatedParams, estimatedParams2;

	//skip header
	getline(in, tmp);

	//read estimated params for RG0 and RG1
	getline(in, tmp);
	getline(in, tmp2);
	coretools::str::fillContainerFromStringAny(tmp, estimatedParams, "\t");
	coretools::str::fillContainerFromStringAny(tmp2, estimatedParams2, "\t");

	//parse true params
	logfile->startIndent("Checking parameter values for pooled read groups RG0 and RG1");
	for(unsigned int i=3; i<estimatedParams.size(); ++i){ //first three are model information
		if(estimatedParams[i] != estimatedParams2[i]){
			logfile->newLine();
			logfile->conclude("esimated value for parameter number ", i, " in RG0: ", estimatedParams[i], " is not the same as in RG1: ", estimatedParams2[i]);
			return false;
		}
		if(estimatedParams[i] != trueParams[i]){
			logfile->newLine();
			logfile->conclude("esimated value for parameter number ", i, ": ", estimatedParams[i], " and true value: ", trueParams[i-1]);
		}
	}
	logfile->done();

	//read estimated params for RG2
	logfile->startIndent("Checking parameter values for RG2");
	getline(in, tmp);
	coretools::str::fillContainerFromStringAny(tmp, estimatedParams, "\t");

	for(unsigned int i=1; i<estimatedParams.size()-1; ++i){ //first one is read group name, last one LL
		if(estimatedParams[i] != trueParams[i]){
			logfile->newLine();
			logfile->conclude("esimated value for parameter number ", i, ": ", estimatedParams[i], " and true value: ", trueParams[i-1]);
		}
	}
	logfile->done();
	return true;
};

//------------------------------------------
//TAtlasTest_BQSRSimulation
//------------------------------------------

TAtlasTest_BQSRSimulation::TAtlasTest_BQSRSimulation():TAtlasTest(){
	_name = "BQSRSimulation";
	filenameTag = _testingPrefix + _name;

	meanQual = -1;;
	sdphredInt = -1.0;
	minPhredInt = -1;
	maxPhredInt = -1;
	phi1 = -1;
	phi2 = -1.0;
	revIntercept = -1.0;
	acceptedDelta = -1.0;
	minReadLen = -1;
	maxReadLen = -1;
	positionEffectSlope = -1.0;
	positionEffectIntercept = -1.0;
};

void TAtlasTest_BQSRSimulation::setVariables(coretools::TParameters & params, coretools::TLog* Logfile, coretools::TTaskList* TaskList){
	logfile = Logfile;
	taskList = TaskList;
	bamFileName = filenameTag + ".bam";
	fastaFileName = filenameTag + ".fasta";
	meanQual = params.get<int>("BQSR_meanQual", 25);
	sdphredInt = params.get("BQSR_sdQual", 10);
	minPhredInt = params.get<int>("BQSR_minQual", 0);
	maxPhredInt = params.get<int>("BQSR_maxQual", 42);
	qualityDist = params.get<std::string>("BQSR_qualityDist", coretools::str::toString("normal(", meanQual, ",", sdphredInt, ")[", minPhredInt, ",", maxPhredInt, "]"));
//	alpha = params.getParameterDoubleWithDefault("alpha", 10.0);
//	beta = params.getParameterDoubleWithDefault("beta", 0.2);
	minReadLen = params.get<int>("BQSR_minReadLen", 30);
	maxReadLen = params.get<int>("BQSR_maxReadLen", 100);
//	readLengthDist = params.get<std::string>("readLength", "gamma(alpha,beta)[min,max]");
	positionEffectSlope = params.get("BQSR_positionEffectSlope", 0.0144928);
	positionEffectIntercept = params.get("BQSR_positionEffectIntercept", 0.485507);
	phi1 = params.get<int>("BQSR_phi1", 35);
	phi2 = params.get("BQSR_phi2", 1.2);
	revIntercept = params.get("BQSR_revIntercept", 1.5);
	acceptedDelta = params.get("BQSR_acceptedDelta", 1);
};

bool TAtlasTest_BQSRSimulation::run(coretools::TParameters & params, coretools::TLog* Logfile, coretools::TTaskList* TaskList){
	//set variables
	setVariables(params, Logfile, TaskList);

	//TODO: find minimal data necessary to run test in order to speed up
	//1) Run ATLAS to simulate BAM file
	//-----------------------------
	_testParams.addParameter("out", filenameTag);
	_testParams.addParameter("qualityDist", qualityDist);
	_testParams.addParameter("chrLength", "5000000");
	_testParams.addParameter("refDiv", "0.0");
	_testParams.addParameter("ploidy", "1");
	_testParams.addParameter("BQSRTransformation", coretools::str::toString("[", phi1, ",", phi2, ",", revIntercept, "]"));
//	_testParams.addParameter("readLength", "gamma(" + toString(alpha) + "," + toString(beta)+ ")[" + toString(minReadLen) + "," + toString(maxReadLen));
	_testParams.addParameter("readLength", "fixed(70)");


	if(!runMain("simulate"))
		return false;

	logfile->newLine();

	//1) Run BQSR
	//-----------------------------
	_testParams.addParameter("bam", bamFileName);
	_testParams.addParameter("fasta", fastaFileName);
	_testParams.addParameter("storeInMemory", "");
	_testParams.addParameter("estimateBQSRPosition", "");
	_testParams.addParameter("maxPos", "110");

	if(!runMain("BQSR"))
		return false;


	//3) check if results are OK
	//--------------------------
	if(checkBQSRQualityFile() == true && checkBQSRPositionFile() == true) return true;
	else return false;
};

double TAtlasTest_BQSRSimulation::trueQual(int & phi1, double & phi2, int & fakeQual){
	double tmpPhi1 = (double) phi1;
	double tmpFakeQual = (double) fakeQual;
	double exp1, exp2;
	exp1 = pow(10.0,-1.0/10.0*phi2*tmpFakeQual);
	exp2 = pow(10.0, -tmpPhi1/10.0);

	double trueQual = -10.0 * log10(exp1 + exp2);
	return trueQual;
};

bool TAtlasTest_BQSRSimulation::checkBQSRQualityFile(){
	logfile->startIndent("Checking BQSR Quality table:");

	//open quality file
	std::string filename = filenameTag + "_BQSR_ReadGroup_Quality_Table.txt";
	logfile->listFlush("Opening file '" + filename + "' for reading ...");
	std::ifstream in(filename.c_str());
	if(!in)
		throw "Failed to open file '" + filename + "'!";
	logfile->done();

	//skip header and quality 0
	std::string tmp;
	getline(in, tmp);
	getline(in, tmp);

	//some variables
	std::vector<std::string> line;
	int numLines = 0;
	int QualityScoreAsPhredInt;
	double EmpiricalQuality;
	double Log10Observations;
	int unacceptablesCount = 0;
	double maxEmpiricQual = 0;

	//parse file line by line check contents
	logfile->listFlush("Parsing file ...");
	while(in.good() && !in.eof()){
		//read line into vector
		++numLines;
		coretools::str::fillContainerFromLineWhiteSpace(in, line, true);
		QualityScoreAsPhredInt = coretools::str::convertString<int>(line[1]);
		EmpiricalQuality = coretools::str::convertString<double>(line[3]);
		Log10Observations = coretools::str::convertString<double>(line[4]);
		if(Log10Observations >= 5.5 && fabs(EmpiricalQuality - trueQual(phi1, phi2, QualityScoreAsPhredInt)) > acceptedDelta){
			++unacceptablesCount;
		}
		if(Log10Observations >= 4.5 && (EmpiricalQuality > maxEmpiricQual)){
			maxEmpiricQual = EmpiricalQuality;
		}
	}
	if(unacceptablesCount > 0){
		logfile->newLine();
		logfile->conclude("There were ", unacceptablesCount, " empirical quality scores that did not match.");
		return false;
	}
	if(fabs(maxEmpiricQual - phi1) > acceptedDelta){
		logfile->newLine();
		logfile->conclude("There is at least one empirical quality scores that was estimated to be larger than phi1.");
		return false;
	}
	logfile->done();
	logfile->endIndent();

	return true;
}

//void TAtlasTest_BQSRSimulation::calculateSlopeIntercept(){
//	std::map<std::string, std::string> readLengthMap;
//
//	double sum = 0.0;
//	//gamma density starts at 0 but p at 1!
//	for(int p=1; p<(maxReadLen + 1) ; ++p)
//		sum += (double) p * readLengthDist->positionProbs[p-1];
//
//	m = (1.0 - revIntercept) / (sum - maxReadLength);
//	intercept = revIntercept - m * maxReadLength;
//
//	if(intercept < 0) throw "The value given for the reverse intercept results in a negative intercept!";
//}

double TAtlasTest_BQSRSimulation::trueScaling(int & pos){
	double trueScaling = positionEffectIntercept + positionEffectSlope * pos;
	return trueScaling;
}

bool TAtlasTest_BQSRSimulation::checkBQSRPositionFile(){
	logfile->startIndent("Checking BQSR Position table:");

	//open quality file
	std::string filename = filenameTag + "_BQSR_ReadGroup_Position_Table.txt";
	logfile->listFlush("Opening file '" + filename + "' for reading ...");
	std::ifstream in(filename.c_str());
	if(!in)
		throw "Failed to open file '" + filename + "'!";
	logfile->done();

	//skip header and quality 0
	std::string tmp;
	getline(in, tmp);
	getline(in, tmp);

	//some variables
	std::vector<std::string> line;
	int numLines = 0;
	int Position;
	double Scaling;
	double Log10Observations;
	int unacceptablesCount = 0;

	//parse file line by line check contents
	logfile->listFlush("Parsing file ...");
	while(in.good() && !in.eof()){
		//read line into vector
		++numLines;
		coretools::str::fillContainerFromLineWhiteSpace(in, line, true);
		Position = coretools::str::convertString<int>(line[1]);
		Scaling = coretools::str::convertString<double>(line[3]);
		Log10Observations = coretools::str::convertString<double>(line[4]);
		if(Log10Observations > 4.5 && fabs(trueScaling(Position) - Scaling) >= 0.1) ++unacceptablesCount;
	}
	if(unacceptablesCount > 0){
		logfile->newLine();
		logfile->conclude("There were ", unacceptablesCount, " scaling factor estimates that did not match.");
		return false;
	}
	logfile->done();
	logfile->endIndent();

	return true;
}

//------------------------------------------
//TAtlasTest_qualityTransformationRecal
//------------------------------------------
/*
TAtlasTest_qualityTransformationRecalPlain::TAtlasTest_qualityTransformationRecalPlain():TAtlasTest(){
	_name = "qualityTransformationPlain";
	filenameTag = _testingPrefix + _name;

	recalObject = nullptr;
	randomGenerator = nullptr;
	qualityDist = nullptr;
	maxReadLength = -1;
};

void TAtlasTest_qualityTransformationRecalPlain::setVariables(TParameters & params, TLog* Logfile, TTaskList* TaskList){
	logfile = Logfile;
	taskList = TaskList;
	bamFileName = filenameTag + ".bam";
	std::string onlyRecalParams = "1,0;0,0;0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
	recalParamString = params.get<std::string>("recal_recalParams","qualFuncPosFuncContext[" + onlyRecalParams +"]");
	maxReadLength = 70;
	randomGenerator = new TRandomGenerator();
	qualDistString = "10";
	qualityDist =  new Simulations::TSimulatorQualityDist(qualDistString);
	recalObject = new Simulations::TSimulatorQualityTransformationRecal("qualFuncPosFuncContext[" + onlyRecalParams + "]", maxReadLength, qualityDist, randomGenerator);

};

bool TAtlasTest_qualityTransformationRecalPlain::run(TParameters & params, TLog* Logfile, TTaskList* TaskList){
	//1) set variables
	setVariables(params, Logfile, TaskList);

	//1) Run ATLAS to simulate BAM file
	//-----------------------------
	_testParams.addParameter("out", filenameTag);
	_testParams.addParameter("chrLength", "2000000");
	_testParams.addParameter("depth", "4");
	_testParams.addParameter("recal", recalParamString);
	_testParams.addParameter("readLength", "single:fixed(" + coretools::str::toString(maxReadLength) + ")");
	_testParams.addParameter("qualityDist", "fixed(" + qualDistString + ")");


	if(!runMain("simulate"))
		return false;

	logfile->newLine();

	//1) Run qualityTransformation
	//-----------------------------
	_testParams.addParameter("bam", bamFileName);
	_testParams.addParameter("recal", recalParamString);


	if(!runMain("qualityTransformation"))
		return false;


	//3) check if results are OK
	//--------------------------
	if(readTransformationFile() == true){
		if(checkTransformation(qualDistVec) == true) return true;
	}
//	else return false;
	return false;
};

bool TAtlasTest_qualityTransformationRecalPlain::readTransformationFile(){
	//open quality file
	std::string filename = filenameTag + "_total_qualityTransformation.txt";
	logfile->listFlush("Opening file '" + filename + "' for parsing ...");
	std::ifstream in(filename.c_str());
	if(!in){
		throw "Failed to open file '" + filename + "'!";
	}
	//parse file line by line check contents
	std::vector<double> tmp;
	while(in.good() && !in.eof()){
		coretools::str::fillContainerFromLineAny(in, tmp, "\t");
		qualTransTable.push_back(tmp);
	}
	logfile->done();
	return true;
}

bool TAtlasTest_qualityTransformationRecalPlain::checkTransformation(std::vector<int> trueQualScores){
	//find true quality scores
	std::vector<int> transformedQualScores;
	int numQualScores = trueQualScores.size();

	transformedQualScores.push_back(19);
	transformedQualScores.push_back(30);
	transformedQualScores.push_back(40);
	transformedQualScores.push_back(60);

	//is the rest = 0?
	double s = 0.0;
	for(unsigned int i=1; i<qualTransTable.size(); ++i){
		for(unsigned int j=1; j<qualTransTable[i].size(); ++j){
			s += qualTransTable[i][j];
		}
	}
	if(s > 1.0001 || s < 0.9999){
		logfile->newLine();
		logfile->conclude("Proportions in qualityTransformation table don't sum to one!");
		return false;
	}

	//are the qualities transformed correctly? lines=transformed, columns=true
	double fracObservationsFound;
	double fracObservationsExpected = (double) (1.0 / (double) numQualScores);
	for(int qI=0; qI<(int) numQualScores; ++qI){
		fracObservationsFound = qualTransTable[trueQualScores[qI]+1][transformedQualScores[qI]+1];
		if( fracObservationsFound < (fracObservationsExpected - 0.0009) || fracObservationsFound > (fracObservationsExpected + 0.0009)){ //+1 for header and line names
			logfile->newLine();
			logfile->conclude("Wrong transformation of ", trueQualScores[qI]+1, "! Found ", fracObservationsFound, " observations in [", transformedQualScores[qI]+1, "][", trueQualScores[qI], "] instead of ", fracObservationsExpected, "!");
			return false;
		}
	}
	return true;
}

//---------------------------------------
TAtlasTest_qualityTransformationRecalBinned::TAtlasTest_qualityTransformationRecalBinned():TAtlasTest_qualityTransformationRecalPlain(){
	_name = "qualityTransformationRecalBinned";
};

void TAtlasTest_qualityTransformationRecalBinned::setVariables(TParameters & params, TLog* Logfile, TTaskList* TaskList){
	logfile = Logfile;
	taskList = TaskList;
	recalParamString = params.get<std::string>("recal_recalParams", "qualFuncPosFuncContext[2,0;0,0;0{20}]");
	qualDistString = "(10,15,20,30)";
	qualityDist =  new Simulations::TSimulatorQualityDistBinned(qualDistString, randomGenerator);
	recalObject = new Simulations::TSimulatorQualityTransformationRecal(recalParamString, maxReadLength, qualityDist, randomGenerator);
	coretools::str::fillContainerFromStringAny(qualDistString, qualDistVec, ",", true);
};

bool TAtlasTest_qualityTransformationRecalBinned::run(TParameters & params, TLog* Logfile, TTaskList* TaskList){
	//1) set variables
	setVariables(params, Logfile, TaskList);

	//1) Run ATLAS to simulate BAM file
	//-----------------------------
	_testParams.addParameter("out", filenameTag);
	_testParams.addParameter("chrLength", "2000000");
	_testParams.addParameter("depth", "2");
	_testParams.addParameter("recalTransformation", recalParamString);
	_testParams.addParameter("readLength", "single:fixed(" + coretools::str::toString(maxReadLength) + ")");
	_testParams.addParameter("qualityDist", "binned(" + qualDistString + ")");

	if(!runMain("simulate"))
		return false;

	logfile->newLine();

	//1) Run qualityTransformation
	//-----------------------------
	_testParams.addParameter("bam", bamFileName);
	_testParams.addParameter("recal", recalParamString);

	if(!runMain("qualityTransformation"))
		return false;

	//3) check if results are OK
	//--------------------------
	if(readTransformationFile() == true){
		if(checkTransformation(qualDistVec) == true)
			return true;
	}
	return false;
};

*/
