//
// Created by caduffm on 10/27/21.
//
#include "TVcfConverter.h"
#include "coretools/Files/TInputFile.h"
#include "coretools/Files/TInputRcpp.h"
#include "coretools/Files/gzstream.h"
#include "coretools/Main/TParameters.h"
#include "coretools/Strings/fromString.h"
#include "coretools/Strings/toString.h"
#include "coretools/Types/probability.h"
#include "genometools/Genotypes/BiallelicGenotype.h"
#include "gtest/gtest.h"


using coretools::instances::parameters;
using coretools::str::toString;
using coretools::str::fromString;
using coretools::Probability;
using coretools::TInputFile;
using coretools::FileType;

using genometools::BiallelicGenotype;
using genometools::TSampleLikelihoods;


class TVCFConverterTest : public testing::Test {
protected:
	std::vector<uint16_t> phred_g1         = {0, 1, 0, 0, 3, 0, 1, 2, 0, 0, 2, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 3, 1,
                                      0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 2, 3, 2, 0, 3, 0, 0, 1, 0, 0, 0, 1, 2, 0};
	std::vector<uint16_t> phred_g2         = {1, 2, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 2, 1, 2, 2, 0, 1, 1, 0, 2, 0, 0, 0,
                                      0, 0, 0, 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1};
	std::vector<uint16_t> phred_g3         = {1, 0, 1, 3, 1, 2, 0, 1, 0, 1, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
                                      1, 3, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 2, 3, 0, 0, 1, 0, 2, 1, 0, 1};
	std::vector<uint16_t> depth            = {14, 11, 9, 9, 6,  8, 12, 9,  10, 11, 5,  14, 7,  13, 13, 11, 11,
                                   5,  9,  8, 9, 8,  6, 7,  10, 14, 10, 11, 13, 10, 7,  9,  11, 13,
                                   8,  7,  9, 6, 10, 6, 15, 15, 10, 9,  11, 9,  6,  14, 9,  14};
	std::vector<std::string> sampleNames   = {"Indiv1", "Indiv2", "Indiv3", "Indiv4", "Indiv5"};
	std::vector<std::string> samplesToKeep = {"Indiv4", "Indiv1", "Indiv5"}; // omit Indiv3 and Indiv4; and shuffle
	std::vector<size_t> indexInSampleNames = {3, 0, 4};

	static void _writeHeader(gz::ogzstream &VCF, const std::vector<std::string> &SampleNames) {
		// write info
		VCF << "##fileformat=VCFv4.2\n";
		VCF << "##source=Simulation\n";
		VCF << "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n";
		VCF << "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n";
		VCF << "##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"Phred-scaled genotype likelihoods\">\n";
		VCF << "##FORMAT=<ID=DP,Number=G,Type=Integer,Description=\"Depth at site\">\n";

		// write header
		VCF << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT";
		for (const auto &SampleName : SampleNames) { VCF << "\t" << SampleName; }
	}

	static void _writeCell(gz::ogzstream &VCF, const TSampleLikelihoods<coretools::PhredInt> &SampleLikelihoods,
	                       size_t Depth) {
		using BG = BiallelicGenotype;

		// genotype with lowest phred-score is the observed genotype
		BG observedGenotype   = SampleLikelihoods.mostLikelyGenotype();
		BG secondBestGenotype = SampleLikelihoods.secondMostLikelyGenotype();

		// write to vcf
		VCF << "\t" << toString(observedGenotype) << ":" << SampleLikelihoods[secondBestGenotype] << ":";
		VCF << toString(SampleLikelihoods[BG::homoFirst]) + "," + toString(SampleLikelihoods[BG::het]) + "," +
		           toString(SampleLikelihoods[BG::homoSecond]);
		VCF << ":" << Depth;
	};

public:
	std::string filename;

	const uint32_t numLoci  = 10;
	const uint32_t numIndiv = 5;

	void SetUp() override {
		parameters().clear();

		filename = "test.vcf.gz";
		parameters().add("vcf", filename);
		parameters().add("minMAF", "0");
	}

	void writeVcfFile() {
		gz::ogzstream vcf;
		vcf.open(filename.c_str());
		_writeHeader(vcf, sampleNames);

		std::string chr    = "junk1";
		size_t linearIndex = 0;
		for (size_t l = 0; l < numLoci; ++l) {
			vcf << '\n' << chr << '\t' << l + 1 << "\t.\tA\tC\t50\t.\t.\tGT:GQ:PL:DP";
			for (size_t i = 0; i < numIndiv; ++i, ++linearIndex) {
				// write to vcf
				auto GTL0 = coretools::PhredInt(phred_g1[linearIndex]);
				auto GTL1 = coretools::PhredInt(phred_g2[linearIndex]);
				auto GTL2 = coretools::PhredInt(phred_g3[linearIndex]);
				_writeCell(vcf, {GTL0, GTL1, GTL2}, depth[linearIndex]);
			}
		}
		vcf << "\n";
		vcf.close();
	}

	void writeSampleFile() {
		coretools::TOutputFile sampleFile("test.samples", {"Samples"});
		for (auto &it : samplesToKeep) { sampleFile.writeln(it); }
		// add to parameters
		parameters().add("samples", "test.samples");
	}
};

TEST_F(TVCFConverterTest, beagle) {
	writeVcfFile();

	// convert to beagle
	VCF::TVcfToBeagle converter;
	converter.run();

	// read beagle
	TInputFile beagle("test.beagle.gz", FileType::Header);
	EXPECT_EQ(beagle.header().at(0), "marker");
	EXPECT_EQ(beagle.header().at(1), "allele1");
	EXPECT_EQ(beagle.header().at(2), "allele2");
	for (size_t i = 0; i < numIndiv; ++i) {
		EXPECT_EQ(beagle.header().at(3 + i * 3), sampleNames[i]);
		EXPECT_EQ(beagle.header().at(3 + i * 3 + 1), sampleNames[i]);
		EXPECT_EQ(beagle.header().at(3 + i * 3 + 2), sampleNames[i]);
	}

	// check if genotype likelihoods are as expected
	size_t linearIndex = 0;
	for (size_t l = 0; l < numLoci; ++l) {
		EXPECT_EQ(beagle.get(0), "junk1_" + toString(l + 1));
		EXPECT_EQ(beagle.get(1), "A");
		EXPECT_EQ(beagle.get(2), "C");
		for (size_t i = 0; i < numIndiv; ++i, ++linearIndex) {
			const double sum = static_cast<Probability>(coretools::PhredInt(phred_g1[linearIndex])).get() +
			                   (Probability)coretools::PhredInt(phred_g2[linearIndex]) +
			                   (Probability)coretools::PhredInt(phred_g3[linearIndex]);
			const double gtl1 = fromString<Probability>(beagle.get(3 + i * 3));
			const double gtl2 = fromString<Probability>(beagle.get(3 + i * 3 + 1));
			const double gtl3 = fromString<Probability>(beagle.get(3 + i * 3 + 2));

			// genotype likelihoods (we loose some precision when reading/writing, so only expect near)
			EXPECT_NEAR(gtl1, (Probability)coretools::PhredInt(phred_g1[linearIndex]) / sum, 0.00001);
			EXPECT_NEAR(gtl2, (Probability)coretools::PhredInt(phred_g2[linearIndex]) / sum, 0.00001);
			EXPECT_NEAR(gtl3, (Probability)coretools::PhredInt(phred_g3[linearIndex]) / sum, 0.00001);
			EXPECT_NEAR(gtl1 + gtl2 + gtl3, 1.0, 0.00001);
		}
		beagle.popFront();
	}
}

TEST_F(TVCFConverterTest, beagle_withSamples) {
	writeVcfFile();
	writeSampleFile();

	// convert to beagle
	VCF::TVcfToBeagle converter;
	converter.run();

	// read beagle
	TInputFile beagle("test.beagle.gz", FileType::Header);
	EXPECT_EQ(beagle.header().at(0), "marker");
	EXPECT_EQ(beagle.header().at(1), "allele1");
	EXPECT_EQ(beagle.header().at(2), "allele2");
	for (size_t i = 0; i < samplesToKeep.size(); ++i) {
		EXPECT_EQ(beagle.header().at(3 + i * 3), samplesToKeep[i]);
		EXPECT_EQ(beagle.header().at(3 + i * 3 + 1), samplesToKeep[i]);
		EXPECT_EQ(beagle.header().at(3 + i * 3 + 2), samplesToKeep[i]);
	}

	// check if genotype likelihoods are as expected
	for (size_t l = 0; l < numLoci; ++l) {
		EXPECT_EQ(beagle.get(0), "junk1_" + toString(l + 1));
		EXPECT_EQ(beagle.get(1), "A");
		EXPECT_EQ(beagle.get(2), "C");
		for (size_t i = 0; i < samplesToKeep.size(); ++i) {
			size_t relevantIndex = l * numIndiv + indexInSampleNames[i];
			// genotype likelihoods (we loose some precision when reading/writing, so only expect near)
			const double sum     = ((Probability)coretools::PhredInt(phred_g1[relevantIndex])).get() +
			                   (Probability)coretools::PhredInt(phred_g2[relevantIndex]) +
			                   (Probability)coretools::PhredInt(phred_g3[relevantIndex]);
			const double gtl1 = fromString<Probability>(beagle.get(3 + i * 3));
			const double gtl2 = fromString<Probability>(beagle.get(3 + i * 3 + 1));
			const double gtl3 = fromString<Probability>(beagle.get(3 + i * 3 + 2));
			EXPECT_NEAR(gtl1, (Probability)coretools::PhredInt(phred_g1[relevantIndex]) / sum, 0.00001);
			EXPECT_NEAR(gtl2, (Probability)coretools::PhredInt(phred_g2[relevantIndex]) / sum, 0.00001);
			EXPECT_NEAR(gtl3, (Probability)coretools::PhredInt(phred_g3[relevantIndex]) / sum, 0.00001);
			EXPECT_NEAR(gtl1 + gtl2 + gtl3, 1.0, 0.00001);
		}
		beagle.popFront();
	}
}

TEST_F(TVCFConverterTest, geno) {
	writeVcfFile();

	// convert to geno
	VCF::TVcfToGeno converter;
	converter.run();

	// read geno
	TInputFile geno("test.geno", FileType::NoHeader);

	// check if genotypes are as expected
	for (size_t l = 0; l < numLoci; ++l) {
		std::vector<char> genotypes(geno.get(0).begin(), geno.get(0).end());
		for (size_t i = 0; i < numIndiv; ++i) {
			// genotypes
			size_t relevantIndex = l * numIndiv + i;
			auto GTL0            = coretools::PhredInt(phred_g1[relevantIndex]);
			auto GTL1            = coretools::PhredInt(phred_g2[relevantIndex]);
			auto GTL2            = coretools::PhredInt(phred_g3[relevantIndex]);
			TSampleLikelihoods sampleLikelihoods(GTL0, GTL1, GTL2);
			BiallelicGenotype observedGenotype = sampleLikelihoods.mostLikelyGenotype();

			EXPECT_EQ(fromString<uint8_t>(std::string(1, genotypes[i])),
			          2 - (uint8_t)observedGenotype); // genotype defined based on reference allele
		}
		geno.popFront();
	}
}

TEST_F(TVCFConverterTest, geno_withSamples) {
	writeVcfFile();
	writeSampleFile();

	// convert to geno
	VCF::TVcfToGeno converter;
	converter.run();

	// read geno
	TInputFile geno("test.geno", FileType::NoHeader);

	// check if genotypes are as expected
	for (size_t l = 0; l < numLoci; ++l) {
		std::vector<char> genotypes(geno.get(0).begin(), geno.get(0).end());
		for (size_t i = 0; i < samplesToKeep.size(); ++i) {
			// genotypes
			size_t relevantIndex = l * numIndiv + indexInSampleNames[i];
			auto GTL0            = coretools::PhredInt(phred_g1[relevantIndex]);
			auto GTL1            = coretools::PhredInt(phred_g2[relevantIndex]);
			auto GTL2            = coretools::PhredInt(phred_g3[relevantIndex]);
			TSampleLikelihoods sampleLikelihoods(GTL0, GTL1, GTL2);
			BiallelicGenotype observedGenotype = sampleLikelihoods.mostLikelyGenotype();

			// genotype defined based on reference allele
			EXPECT_EQ(fromString<uint8_t>(std::string(1, genotypes[i])), 2 - (uint8_t)observedGenotype);
		}
		geno.popFront();
	}
}

TEST_F(TVCFConverterTest, lfmmCalledGeno) {
	writeVcfFile();

	// convert to lfmm
	VCF::TVcfToLFMM<true> converter;
	converter.run();

	// read lfmm
	TInputFile lfmm("test.lfmm", FileType::NoHeader);

	// check if genotypes are as expected
	for (size_t i = 0; i < numIndiv; ++i) {
		for (size_t l = 0; l < numLoci; ++l) {
			// genotypes
			size_t relevantIndex = l * numIndiv + i;
			auto GTL0            = coretools::PhredInt(phred_g1[relevantIndex]);
			auto GTL1            = coretools::PhredInt(phred_g2[relevantIndex]);
			auto GTL2            = coretools::PhredInt(phred_g3[relevantIndex]);
			TSampleLikelihoods sampleLikelihoods(GTL0, GTL1, GTL2);
			BiallelicGenotype observedGenotype = sampleLikelihoods.mostLikelyGenotype();

			EXPECT_EQ(fromString<uint8_t>(lfmm.get(l)), (uint8_t)observedGenotype);
		}
		lfmm.popFront();
	}
}

TEST_F(TVCFConverterTest, lfmmCalledGeno_withSamples) {
	writeVcfFile();
	writeSampleFile();

	// convert to lfmm
	VCF::TVcfToLFMM<true> converter;
	converter.run();

	// read lfmm
	TInputFile lfmm("test.lfmm", FileType::NoHeader);

	// check if genotypes are as expected
	for (size_t i = 0; i < samplesToKeep.size(); ++i) {
		for (size_t l = 0; l < numLoci; ++l) {
			// genotypes
			size_t relevantIndex = l * numIndiv + indexInSampleNames[i];
			auto GTL0            = coretools::PhredInt(phred_g1[relevantIndex]);
			auto GTL1            = coretools::PhredInt(phred_g2[relevantIndex]);
			auto GTL2            = coretools::PhredInt(phred_g3[relevantIndex]);
			TSampleLikelihoods sampleLikelihoods(GTL0, GTL1, GTL2);
			BiallelicGenotype observedGenotype = sampleLikelihoods.mostLikelyGenotype();

			EXPECT_EQ(fromString<uint8_t>(lfmm.get(l)), (uint8_t)observedGenotype);
		}
		lfmm.popFront();
	}
}

TEST_F(TVCFConverterTest, lfmmMeanPosteriorGeno) {
	writeVcfFile();

	// convert to lfmm
	VCF::TVcfToLFMM<false> converter;
	converter.run();

	// read lfmm
	TInputFile lfmm("test.lfmm", FileType::NoHeader);

	// check if genotype likelihoods are as expected
	for (size_t i = 0; i < numIndiv; ++i) {
		for (size_t l = 0; l < numLoci; ++l) {
			// genotypes
			size_t relevantIndex = l * numIndiv + i;
			auto GTL0            = coretools::PhredInt(phred_g1[relevantIndex]);
			auto GTL1            = coretools::PhredInt(phred_g2[relevantIndex]);
			auto GTL2            = coretools::PhredInt(phred_g3[relevantIndex]);
			TSampleLikelihoods sampleLikelihoods(GTL0, GTL1, GTL2);
			double posteriorGenotype = sampleLikelihoods.meanPosteriorGenotype();

			EXPECT_NEAR(fromString<double>(lfmm.get(l)), posteriorGenotype, 0.00001);
		}
		lfmm.popFront();
	}
}

TEST_F(TVCFConverterTest, lfmmMeanPosteriorGeno_withSamples) {
	writeVcfFile();
	writeSampleFile();

	// convert to lfmm
	VCF::TVcfToLFMM<false> converter;
	converter.run();

	// read lfmm
	TInputFile lfmm("test.lfmm", FileType::NoHeader);

	// check if genotypes are as expected
	for (size_t i = 0; i < samplesToKeep.size(); ++i) {
		for (size_t l = 0; l < numLoci; ++l) {
			// genotypes
			size_t relevantIndex = l * numIndiv + indexInSampleNames[i];
			auto GTL0            = coretools::PhredInt(phred_g1[relevantIndex]);
			auto GTL1            = coretools::PhredInt(phred_g2[relevantIndex]);
			auto GTL2            = coretools::PhredInt(phred_g3[relevantIndex]);
			TSampleLikelihoods sampleLikelihoods(GTL0, GTL1, GTL2);
			double posteriorGenotype = sampleLikelihoods.meanPosteriorGenotype();

			EXPECT_NEAR(fromString<double>(lfmm.get(l)), posteriorGenotype, 0.00001);
		}
		lfmm.popFront();
	}
}

TEST_F(TVCFConverterTest, sambada) {
	writeVcfFile();

	// convert to lfmm
	VCF::TVcfToSambada converter;
	converter.run();

	// read lfmm
	TInputFile sambada("test.sambada", FileType::Header);

	// check if genotypes are as expected
	for (size_t i = 0; i < numIndiv; ++i) {
		EXPECT_EQ(sambada.get(0), sampleNames[i]);
		for (size_t l = 0; l < numLoci; ++l) {
			// genotypes
			size_t relevantIndex = l * numIndiv + i;
			auto GTL0            = coretools::PhredInt(phred_g1[relevantIndex]);
			auto GTL1            = coretools::PhredInt(phred_g2[relevantIndex]);
			auto GTL2            = coretools::PhredInt(phred_g3[relevantIndex]);
			TSampleLikelihoods sampleLikelihoods(GTL0, GTL1, GTL2);
			BiallelicGenotype observedGenotype = sampleLikelihoods.mostLikelyGenotype();

			for (size_t g = 0; g < 3; ++g) {
				if (g == (uint8_t)observedGenotype) {
					EXPECT_EQ(fromString<size_t>(sambada.get(3 * l + g + 1)), 1);
				} else {
					EXPECT_EQ(fromString<size_t>(sambada.get(3 * l + g + 1)), 0);
				}
			}
		}
		sambada.popFront();
	}
}

TEST_F(TVCFConverterTest, sambada_withSamples) {
	writeVcfFile();
	writeSampleFile();

	// convert to lfmm
	VCF::TVcfToSambada converter;
	converter.run();

	// read sambada
	TInputFile sambada("test.sambada", FileType::Header);

	// check if genotypes are as expected
	for (size_t i = 0; i < samplesToKeep.size(); ++i) {
		EXPECT_EQ(sambada.get(0), samplesToKeep[i]);
		for (size_t l = 0; l < numLoci; ++l) {
			// genotypes
			size_t relevantIndex = l * numIndiv + indexInSampleNames[i];
			auto GTL0            = coretools::PhredInt(phred_g1[relevantIndex]);
			auto GTL1            = coretools::PhredInt(phred_g2[relevantIndex]);
			auto GTL2            = coretools::PhredInt(phred_g3[relevantIndex]);
			TSampleLikelihoods sampleLikelihoods(GTL0, GTL1, GTL2);
			BiallelicGenotype observedGenotype = sampleLikelihoods.mostLikelyGenotype();

			for (size_t g = 0; g < 3; ++g) {
				if (g == (uint8_t)observedGenotype) {
					EXPECT_EQ(fromString<uint8_t>(sambada.get(3 * l + g + 1)), 1);
				} else {
					EXPECT_EQ(fromString<uint8_t>(sambada.get(3 * l + g + 1)), 0);
				}
			}
		}
		sambada.popFront();
	}
}

TEST_F(TVCFConverterTest, vcfToPosFile) {
	writeVcfFile();
	writeSampleFile();

	// convert to lfmm
	VCF::TVcfToPosFile converter;
	converter.run();

	// read pos file
	TInputFile pos("test.pos", FileType::NoHeader);

	// check if positions are as expected
	for (size_t l = 0; l < numLoci; ++l) {
		EXPECT_EQ(pos.get(0), "junk1");
		EXPECT_EQ(pos.get(1), toString(l + 1));
		EXPECT_EQ(pos.get(2), "A");
		EXPECT_EQ(pos.get(3), "C");
		pos.popFront();
	}
}
