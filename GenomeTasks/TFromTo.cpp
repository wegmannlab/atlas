#include "TFromTo.h"
#include "TSequencedData.h"
#include "coretools/Main/TParameters.h"
#include "coretools/Main/TRandomGenerator.h"
#include "genometools/Genotypes/Base.h"

namespace GenomeTasks{
using coretools::instances::randomGenerator;
using genometools::Base;

TFromTo::TFromTo() : _out(_genome.outputName() + "_fromto.txt.gz") {
	_windows.requireReference();
	_out.writeHeader({"Chr", "Pos", "Depth", "dist5_1", "dist3_1", "dist5_2", "dist3_2", "reveresed1", "reversed2"});
}

void TFromTo::run() { _traverseBAMWindows(); }


void TFromTo::_handleWindow(GenotypeLikelihoods::TWindow& window) {
	using BAM::Flags;
	for (size_t i = 0; i < window.size(); ++i) {
		const auto & site = window[i];
		if (site.empty()) continue;

		const auto r = site.refBase;
		if (r == Base::C || r == Base::G) continue;

		const auto pos = window.position(i);
		std::vector<BAM::TSequencedData> data;
		for (const auto& d: site) {
			if (d.base != r) data.push_back(d);
		}
		if (data.size() < 2 || data.size() > site.depth()/2) continue;

		const auto i1 = randomGenerator().getRand(size_t{}, data.size());
		auto i2       = i1;
		while (i2 == i1) i2 = randomGenerator().getRand(size_t{}, data.size());

		_out.writeln(window.chrName(), pos.position(), site.depth(), data[i1].distFrom5.linear(), data[i1].distFrom3.linear(),
					 data[i2].distFrom5.linear(), data[i2].distFrom3.linear(), data[i1].get<Flags::ReversedStrand>(),
					 data[i2].get<Flags::ReversedStrand>());
	}
}
} // namespace GenomeTasks
