/*
 * THaplotypeSimulator.h
 *
 *  Created on: Apr 7, 2017
 *      Author: phaentu
 */

#ifndef THAPLOTYPESIMULATOR_H_
#define THAPLOTYPESIMULATOR_H_

#include <array>
#include <memory>
#include <string>
#include <vector>

#include "TSimulatorHaplotypes.h"
#include "TSimulatorMutationtable.h"
#include "coretools/Containers/TStrongArray.h"
#include "coretools/Files/TOutputFile.h"
#include "coretools/Types/probability.h"

#include "SFS.h"
#include "genometools/Genotypes/Containers.h"


namespace genometools { class TChromosome; }
namespace genometools { class TChromosomes; }

namespace Simulations {
class TSimulatorHaplotypes;

class THaplotypeSimulator {
protected:
	genometools::TBaseProbabilities _baseFreq;
	coretools::TStrongArray<double, genometools::Base> _cumulBaseFreq;
	coretools::Probability _referenceN;
	THaplotypeSimulator();
public:
	virtual ~THaplotypeSimulator() = default;
	virtual void simulateDiploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference, const genometools::TChromosome &chromosome) = 0;
	virtual void simulateHaploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference, const genometools::TChromosome &chromosome) = 0;

	[[nodiscard]] virtual bool simulatesBiallelic() const noexcept = 0;
	[[nodiscard]] virtual size_t sampleSize() const noexcept = 0;
};

class THaplotypeRefDivSimulator : public THaplotypeSimulator {
protected:
	coretools::TStrongArray<double, genometools::Base> _cumulRef; // N is a posibility
	coretools::Probability _referenceDivergence;
public:
	THaplotypeRefDivSimulator();
};

//---------------------------------------------------------
// TSimulatorOneIndividual
//---------------------------------------------------------
class TSimulatorOne final : public THaplotypeRefDivSimulator {
private:
	std::vector<double> _thetas;

public:
	TSimulatorOne(size_t nChoromosomes);
	static inline const std::string name = "one";

	void simulateDiploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference,
					const genometools::TChromosome &chromosome) override;
	void simulateHaploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference,
					const genometools::TChromosome &chromosome) override;

	[[nodiscard]] bool simulatesBiallelic() const noexcept override { return true; };
	[[nodiscard]] size_t sampleSize() const noexcept override {return 1;}
};

class TSimulatorHKY85 final : public THaplotypeSimulator {
private:
	using Pickers = std::vector<coretools::TStrongArray<coretools::TRandomPicker, genometools::Base>>;

	Pickers _pick_g;
	Pickers _pick_r;

public:
	TSimulatorHKY85(size_t nChoromosomes);
	static inline const std::string name = "HKY85";

	void simulateDiploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference,
					const genometools::TChromosome &chromosome) override;
	void simulateHaploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference,
					const genometools::TChromosome &chromosome) override;

	[[nodiscard]] bool simulatesBiallelic() const noexcept override { return true; };
	[[nodiscard]] size_t sampleSize() const noexcept override {return 1;}
};

//---------------------------------------------------------
// TSimulatorPairOfIndividuals
//---------------------------------------------------------
class TSimulatorPair final : public THaplotypeRefDivSimulator {
private:
	static constexpr std::array<std::array<size_t, 4>, 4> _orderLookup = {
	    {{0, 1, 2, 3}, {0, 1, 3, 2}, {1, 0, 2, 3}, {1, 0, 3, 2}}};
	std::vector<double> _phis;
	std::array<double, 9> _cumulGenoCaseFrequencies;
	std::array<std::vector<double>, 9> _cumulGenoCombinationFreq;
	std::array<std::vector<std::array<genometools::Base, 4>>, 9> _genoTrans;

	void _fillTables();

public:
	TSimulatorPair();
	static inline const std::string name = "pair";
	void simulateDiploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference,
					const genometools::TChromosome &chromosome) override;
	void simulateHaploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference,
					const genometools::TChromosome &chromosome) override;

	[[nodiscard]] bool simulatesBiallelic() const noexcept override { return false; };
	[[nodiscard]] size_t sampleSize() const noexcept override {return 2;}
};

//---------------------------------------------------------
// TSimulatorSFS
//---------------------------------------------------------
class TSimulatorSFS final : public THaplotypeRefDivSimulator {
private:
	int _sampleSize;
	std::vector<std::unique_ptr<SFS>> _sfs;
	TSimulatorMutationtable _mutTable;

	void _initializeSFS(const genometools::TChromosomes& chromosomes, const std::vector<double> &thetas, bool folded);
	void _initializeSFS(const genometools::TChromosomes& chromosomes, const std::vector<std::string> &sfsFileNames, bool folded);
public:
	TSimulatorSFS(const genometools::TChromosomes& chromosomes);
	static inline const std::string name = "SFS";
	void simulateHaploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference,
					const genometools::TChromosome &chromosome) override;
	void simulateDiploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference,
					const genometools::TChromosome &chromosome) override;

	[[nodiscard]] bool simulatesBiallelic() const noexcept override { return true; };
	[[nodiscard]] size_t sampleSize() const noexcept override {return _sampleSize;}
};

//---------------------------------------------------------
// TSimulatorHardyWeinberg
//---------------------------------------------------------
struct TSimulatorHWSite {
	bool isPolymorphic;
	genometools::Base reference;
	genometools::Base alternative;
	double f;
};

class TSimulatorHW : public THaplotypeRefDivSimulator {
private:
	int _sampleSize;
	coretools::Probability _fracPoly;
	double _alpha;
	double _beta;
	double _F;
	std::array<double, 3> _cumulGenoProb;
	TSimulatorMutationtable _mutTable;
	bool _writeTrueAlleleFreq = false;
	coretools::TOutputFile _trueFreqFile;

	void _fillCumulGenoProb(double f);
	void _simulateSite(TSimulatorHWSite &site, TSimulatorReference &reference, const std::string &chr, size_t pos);
	void _fillhaplotypesMonomoprhic(TSimulatorHaplotypes &haplotypes, size_t locus, const TSimulatorHWSite &site);

public:
	TSimulatorHW();
	static inline const std::string name = "HW";
	void simulateHaploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference,
					const genometools::TChromosome &chromosome) override;
	void simulateDiploid(TSimulatorHaplotypes &haplotypes, TSimulatorReference &reference,
	                                const genometools::TChromosome &chromosome) override;

	[[nodiscard]] bool simulatesBiallelic() const noexcept override { return true; };
	[[nodiscard]] size_t sampleSize() const noexcept override {return _sampleSize;}
};
} // namespace Simulations

#endif
